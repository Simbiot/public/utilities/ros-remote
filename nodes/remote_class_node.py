#!/usr/bin/env python
import rospy
from pynput import keyboard
import std_msgs

class keyboard_listener:
    def __init__(self):
        rospy.init_node('keyboard_listener', anonymous=True)
        topic_name = rospy.get_param('topic_name', 'remote_says_coucou')
        queue_size = rospy.get_param('queue_size', 10)
        msg_type = rospy.get_param('msg_type', std_msgs.msg.String)
        self.pub = rospy.Publisher(topic_name, msg_type, queue_size=queue_size)
        self.listener = keyboard.Listener(
            on_press=self.on_press,
            on_release=self.on_release)

    def run(self, rate=100):
        self.listener.start()
        rospy.loginfo('ready to read keyboard inputs')
        r = rospy.Rate(rate) # 100hz
        while not rospy.is_shutdown() and self.listener.running:
            r.sleep()

    def on_press(self, key):
        #rospy.loginfo(key)
        self.pub.publish(str(key))

    def on_release(self, key):
        #rospy.loginfo('\n {0} released'.format(
        #    key))
        if key == keyboard.Key.esc:
            # Stop listener
            return False

if __name__ == '__main__':
    kl = keyboard_listener()
    kl.run()
